<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OctoberBB extends Model
{
    protected $table = 'October2021_BB';
    public $timestamps = false;
    protected $primaryKey = 'NIN_Account';
    public $incrementing = false;
}

