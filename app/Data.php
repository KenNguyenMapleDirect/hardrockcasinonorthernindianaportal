<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Data extends Model
{
    protected $table = 'Datas';
    public $timestamps = false;
    protected $primaryKey = 'NIN_Account';
    public $incrementing = false;
}

