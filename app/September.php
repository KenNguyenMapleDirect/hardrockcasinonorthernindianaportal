<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class September extends Model
{
    protected $table = 'September2021';
    public $timestamps = false;
    protected $primaryKey = 'NIN_Account';
    public $incrementing = false;
}

