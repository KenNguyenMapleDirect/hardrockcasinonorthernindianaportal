<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class November extends Model
{
    protected $table = 'November2021';
    public $timestamps = false;
    protected $primaryKey = 'NIN_Account';
    public $incrementing = false;
}

