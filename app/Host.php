<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Host extends Model
{
    protected $table = 'Host';
    public $timestamps = false;
    protected $primaryKey = 'Host_Name';
    public $incrementing = false;
}

