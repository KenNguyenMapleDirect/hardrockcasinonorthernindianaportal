<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class August extends Model
{
    protected $table = 'August2021';
    public $timestamps = false;
    protected $primaryKey = 'NIN_Account';
    public $incrementing = false;
}

