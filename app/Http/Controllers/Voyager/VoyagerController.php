<?php

namespace App\Http\Controllers\Voyager;

use App\Data;
use App\August;
use App\Host;
use App\November;
use App\October;
use App\OctoberBB;
use App\September;
use App\User;
use App\WeekenderFlipbook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;
use Carbon\Carbon;

class VoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerController
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function makeBB($accountId, $bb)
    {
        $dataImageBody = getimagesize($bb->NIN_Link_Body);
        $dataImageDate = getimagesize($bb->NIN_Link_Date);
        $dataImageAmount = getimagesize($bb->NIN_Link_Amount);
        $height = $dataImageBody[1] + $dataImageDate[1] + $dataImageAmount[1];
        $img = Image::canvas(700, $height);
        $img->fill(Image::make($bb->NIN_Link_Body), 0, 0);
        // paste another image
        $img->insert($bb->NIN_Link_Amount, 'top-left', 0, $dataImageBody[1]);
        $img->insert($bb->NIN_Link_Date, 'bottom');

        if (!File::exists('BB/October/' . $accountId)) {
            File::makeDirectory('BB/October/' . $accountId, $mode = 0777, true, true);
            $img->save(public_path('BB/October/' . $accountId . '/' . $bb->NIN_Job_Number . '.jpg'));
        } else {
//            if (!File::exists('BB/October/' . $accountId . '/' . $bb->NIN_Job_Number . '.jpg'))
            {
                $img->save(public_path('BB/October/' . $accountId . '/' . $bb->NIN_Job_Number . '.jpg'));
            }
        }
    }

    public function getDataByAccountId($accountId)
    {
        //define data
        $data = new \stdClass();
        $data->first_name = '';
        $data->last_name = '';
        $data->currentTier = '';
        $data->comp_Dollar_Balance = 0;
        $data->tierCredits = 0;
        $data->AsOf_Date = null;
        $data->Flipbook_Account = '';
        $data->Weekender_Account = '';
        $data->updated_at = now();

        //FLAG TO CHECK IF HAVE DATA
        $data->flgCoreSM = 0;

        //COUNT TO CALC DIV
        $data->countCoreSM = 0;

        //FLAG TO CHECK BB
        $data->flgBB = 0;

        //define host data
        $defaultHostData = Host::where('Host_Name', 'Default')->first();
        if ($defaultHostData) {
            $data->Host = $defaultHostData->Host_Name;
            $data->hostMarketingName = $defaultHostData->Marketing_Name;
            $data->hostTitle = $defaultHostData->Title;
            $data->hostPhone = $defaultHostData->Phone;
            $data->hostEmail = $defaultHostData->Email;
            $data->hostImage = 'https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/host_images/' .
                str_replace(' ', '_', $data->Host) .
                '.jpg';
        } else {
            $data->Host = '';
            $data->hostMarketingName = '';
            $data->hostTitle = '';
            $data->hostPhone = '';
            $data->hostEmail = '';
            $data->hostImage = 'https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/host_images/Default.jpg';
        }

        //Get User from user Id
        $user = User::where('account_id', $accountId)->first();
        if ($user) {
            $data->first_name = $user->first_name;
            $data->last_name = $user->last_name;
        }
        $data->NIN_Account = $accountId;
        //get user data from Data
        $datas = Data::where('NIN_Account', $accountId)->first();
        if ($datas) {
            if (!$data->first_name)
                $data->first_name = $datas->NIN_FName;
            if (!$data->last_name)
                $data->last_name = $datas->NIN_LName;
            $data->currentTier = $datas->NIN_Card_Level;
            $data->comp_Dollar_Balance = $datas->NIN_Comp_Dollar_Bal;
            $data->tierCredits = $datas->NIN_Tier_Balance;
            $data->AsOf_Date = $datas->NIN_AsOf_Date;

            //get data for host
            $hostData = Host::where('Host_Name', $datas->NIN_Host_Name)->first();
            if ($hostData) {
                $data->Host = $hostData->Host_Name;
                $data->hostMarketingName = $hostData->Marketing_Name;
                $data->hostTitle = $hostData->Title;
                $data->hostPhone = $hostData->Phone;
                $data->hostEmail = $hostData->Email;
                $data->hostImage = 'https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/host_images/' .
                    str_replace(' ', '_', $data->Host) .
                    '.jpg';
            }
        } else {
            $data->userBanned = 1;
        }

        //get bb data from bb table
        $today = Carbon::now();
        $data->octoberBB = OctoberBB::where('NIN_Account', $accountId)->whereDate('NIN_Offer_End_Date','>=',$today)->get();
        if (count($data->octoberBB) > 0) {
            $data->flgBB = 1;
            //make image for BB
            foreach ($data->octoberBB as $octoberBB) {
                $this->makeBB($accountId, $octoberBB);
                $octoberBB->imgLink = '/BB/October/' . $accountId . '/' . $octoberBB->NIN_Job_Number . '.jpg';
            }
        }

        //Get data from October2021 table
        //Define data type flag
        $data->octoberCoreSM = 0;
        $flipbookData = October::where('NIN_Account', $accountId)->get();

        if ($flipbookData) {

            foreach ($flipbookData as $singleDataFromOctober2021) {

                if ($singleDataFromOctober2021->NIN_Mailer_Type === "CORE_SM") {
                    $data->octoberCoreSM = 0;
                    $data->flgCoreSM = 0;
//                    $data->countCoreSM++;
                    $data->octoberCoreSMResult1 = $singleDataFromOctober2021->NIN_Img_Page01 . ".jpg";
                    $data->octoberCoreSMResult2 = $singleDataFromOctober2021->NIN_Img_Page02 . ".jpg";
                    $data->octoberCoreSMResult3 = $singleDataFromOctober2021->NIN_Img_Page03 . ".jpg";
                    $data->octoberCoreSMResult4 = $singleDataFromOctober2021->NIN_Img_Page04 . ".jpg";
                    $data->octoberCoreSMResult5 = $singleDataFromOctober2021->NIN_Img_Page05 . ".jpg";
                    $data->octoberCoreSMResult6 = $singleDataFromOctober2021->NIN_Img_Page06 . ".jpg";
                    $data->octoberCoreSMResult7 = $singleDataFromOctober2021->NIN_Img_Page07 . ".jpg";
                    $data->octoberCoreSMResult8 = $singleDataFromOctober2021->NIN_Img_Page08 . ".jpg";
                }
            }
        }
        //Get data from October2021 table
        //Define data type flag
        $data->novemberCoreSM = 0;
        $flipbookData = November::where('NIN_Account', $accountId)->get();

        if ($flipbookData) {

            foreach ($flipbookData as $singleDataFromOctober2021) {

                if ($singleDataFromOctober2021->NIN_Mailer_Type === "CORE_SM") {
                    $data->novemberCoreSM = 1;
                    $data->flgCoreSM = 1;
                    $data->countCoreSM++;
                    $data->novemberCoreSMResult1 = $singleDataFromOctober2021->NIN_Img_Page01 . ".jpg";
                    $data->novemberCoreSMResult2 = $singleDataFromOctober2021->NIN_Img_Page02 . ".jpg";
                    $data->novemberCoreSMResult3 = $singleDataFromOctober2021->NIN_Img_Page03 . ".jpg";
                    $data->novemberCoreSMResult4 = $singleDataFromOctober2021->NIN_Img_Page04 . ".jpg";
                    $data->novemberCoreSMResult5 = $singleDataFromOctober2021->NIN_Img_Page05 . ".jpg";
                    $data->novemberCoreSMResult6 = $singleDataFromOctober2021->NIN_Img_Page06 . ".jpg";
                    $data->novemberCoreSMResult7 = $singleDataFromOctober2021->NIN_Img_Page07 . ".jpg";
                    $data->novemberCoreSMResult8 = $singleDataFromOctober2021->NIN_Img_Page08 . ".jpg";
                }
            }
        }

        return $data;
    }

    public function index()
    {
        if (Auth::user()->role_id === 1 || Auth::user()->role_id === 4)
            return Voyager::view('voyager::index');
        else {
            //Get User from user Id
            $user = User::where('id', Auth::user()->id)->first();
            $user->Email_Verified = 'Y';
            $user->save();
            $data = $this->getDataByAccountId(Auth::user()->account_id);
            return view('player-dashboard')->with('data', $data);
        }
    }

    //code for superUser here
    public function getViewPlayerDashBoardByAccountId($accountId)
    {
        $data = $this->getDataByAccountId($accountId);
        return view('player-dashboard')->with('data', $data);

    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }
}
