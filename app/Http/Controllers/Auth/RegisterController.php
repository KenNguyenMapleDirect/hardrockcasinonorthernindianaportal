<?php

namespace App\Http\Controllers\Auth;

use App\Data;
use App\August;
use App\Http\Controllers\Controller;
use App\Mail\WelcomeMail;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Validation\ValidationException;
use Mail;

class RegisterController extends Controller
{
    public function register()
    {

        return view('auth.register');
    }


    //verify and set opt in = Y
    public function verifyAndSetOptIn(Request $request)
    {
        $accountId = $request->input('account_id');
        $data = new \stdClass();
        //Get User from $accountId
        $user = User::where('account_id', $accountId)->first();
        $data->message = '';
        $data->status = false;
        if (!$user) {
            $data->message = 'Your account Id does not match our records. Please contact support!';
        } else {
            if($user->Email_Opt_In ==='Y' && $user->Email_Verified === 'Y' && $user->email_verified_at !== NULL)
            {
                $data->message = 'Your account is already verified. Click button bellow to login to the Player Portal.';
                $data->status = true;
            }
            else{
                $data->message = 'Your account is verified. Click here to login to the Player Portal.';
                $data->status = true;
                $user->Email_Verified = 'Y';
                $user->Email_Opt_In = 'Y';
                $user->email_verified_at = now();
                $user->save();
            }
        }
        return view('auth.verified-via-link')->with('data', $data);
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'user_name' => 'required|string|max:255|unique:users',
            'birthday' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'account_id' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);
        $month = date("m", strtotime($request->birthday));
        $day = date("d", strtotime($request->birthday));
        $monthDay = $month . $day;
        //check if user register right birthday and account_id
        $checkAccount = Data::where('NIN_Account', $request->account_id)
            ->where('NIN_DOB_MD', $monthDay)->first();
        if (!$checkAccount) {
            throw ValidationException::withMessages(['No_Match_Up' => 'Your Account Combined ID or Birthday does not match our records
. Please contact guest services.']);
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'role_id' => 2,
            'password' => Hash::make($request->password),
            'account_id' => $request->account_id,
            'DOB' => $monthDay,
            'Email_Opt_In' => 'Y',
        ]);
        $user->sendEmailVerificationNotification();
        return redirect('admin/login')->with('message', 'Register Completed! Please check your mailbox and confirm your email.');
    }

}
