<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class October extends Model
{
    protected $table = 'October2021';
    public $timestamps = false;
    protected $primaryKey = 'NIN_Account';
    public $incrementing = false;
}

