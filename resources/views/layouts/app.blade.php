<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <script type="text/javascript" src="{{asset('HeaderFooterAssets/js/jquery-1.12.4.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/vendor-modernizr.js" id="modernizr-js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/js-foundation.min.js" id="foundation-js-js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/js-scripts.js" id="site-js-js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/js-jquery.bxslider.js" id="bx-slider-js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/js-isotope.pkgd.min.js" id="isotope-js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/js-wp-embed.min.js" id="wp-embed-js"></script>
    <script type="text/javascript" src="/HeaderFooterAssets/js/min-soliloquy-min.js"
            id="soliloquy-lite-script-js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link
        href="https://www.hardrockcasinonorthernindiana.com/-/media/feature/experience-accelerator/bootstrap/bootstrap/styles/optimized-min.css?rev=47abd0c853044730bae334807ab277bb&amp;t=20201009T153503Z"
        rel="stylesheet"/>
    <link
        href="https://www.hardrockcasinonorthernindiana.com/-/media/base-themes/main-theme/styles/optimized-min.css?rev=10e0f485eb1b4f019cfeea737cd4837b&amp;t=20201009T153508Z"
        rel="stylesheet"/>
    <link
        href="/HeaderFooterAssets/css/optimized-min.css"
        rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
    <meta name="VIcurrentDateTime" content="637583368260188910"/>
    <link rel="apple-touch-icon" sizes="57x57"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage"
          content="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#000000">

    <style>
        .is-hardrock-tampa-hollywood-hri .main-header-top__link--highlight, .is-hardrock-tampa-hollywood-hri .main-header-wildcard .component.link {
            background: #59558e !important;
        }

        .is-hardrock-tampa-hollywood-hri .main-header-top__link--highlight:hover, .is-hardrock-tampa-hollywood-hri .main-header-wildcard .component.link:hover {
            background: #1e1e1e !important;
        }
    </style>


    <title>Hard Rock Casino Northern Indiana</title>


    <link rel="canonical" href="https://www.hardrockcasinonorthernindiana.com/"/>
    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/jquery-ui.css"> <!--  used for progressbar -->
    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/mos.css">
    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/eStatement.css">
    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/HRCINIndiana.css">

    <link
        href="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/favicon-96x96.png?rev=2dc13967d83146eca84cc33e271258e3"
        rel="shortcut icon"/>


    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Home"/>
    <meta property="og:url" content="https://www.hardrockcasinonorthernindiana.com/"/>


    <meta name="description"
          content="Press your luck and win big at Hard Rock Casino Northern Indiana in Gary near Chicago."/>


    <meta property="twitter:title" content="Home"/>
    <meta property="twitter:card" content="summary"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>


    <script src="https://polyfill.io/v3/polyfill.min.js"></script>
    <link rel="stylesheet" id="wp-block-library-css" href="/HeaderFooterAssets/css/block-library-style.min.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="normalize-css-css" href="/HeaderFooterAssets/css/css-normalize.min.css" type="text/css"
          media="all">
    {{--    <link rel="stylesheet" id="foundation-css-css" href="/HeaderFooterAssets/css/css-foundation.min.css" type="text/css">--}}
    <link rel="stylesheet" id="site-css-css" href="/HeaderFooterAssets/css/css-style.css" type="text/css" media="all">
    <link rel="stylesheet" id="site-css-css" href="/HeaderFooterAssets/css/host.css" type="text/css" media="all">


</head>
<body class="default-device bodyclass is-hardrock-tampa-hollywood is-hardrock-tampa-hollywood-hri">
<div id="flipbook-section">
    <div id="flipbook-section1">
        <div id="flipbook-section2">
            <div id="weekender-section">
                <!-- GTM Container -->

                <!-- Start of/ Master 02 -->
                <!-- End of/ Master 02 -->


                <!-- #wrapper -->
                <div id="wrapper">
                    <!-- #header -->
                    <div class="sticky-wrapper">
                        <header class="main-header">
                            <div id="header" class="">
                                <div class="row">

                                    <div class="component banners"
                                         data-uniqueid="banners-328b52ba4ab04bd29a31eab501d08fc0">
                                        <div class="component-content">
                                            <ul class="items" style="margin:0 !important;font-size: 14px;">
                                                <li class="item">
                                                    <div style="background-color:#D6D6D6;color:#000"
                                                         data-banner-id="banner_{8B009EFD-4390-481A-9393-EE3AC4E4CFBC}">
                                                        <div class="item-content container">
                                                            <strong><em class="fa fa-exclamation-circle"></em>
                                                                Safe+Sound</strong>:
                                                            We've implemented new safety precautions to ensure your
                                                            safety and comfort.
                                                            <a href="https://www.hardrockcasinonorthernindiana.com/safe-and-sound">Learn
                                                                More</a>.
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                    <div class="component container-fluid main-header-top"
                                         data-uniqueid="container_fluid-58c211cc5b894b4aa3f5c873b650b13b">
                                        <div class="component-content">


                                            <div class="component container main-header__inner"
                                                 data-uniqueid="container-d95a84e0b9314b31ba9057f8be536bb1">
                                                <div class="component-content">

                                                    <div
                                                        class="component logo-image file-type-icon-media-link image main-header-logo--sticky main-header-logo"
                                                        data-uniqueid="logo_image-1b10c91ccecf454fb7aa4678c3ef7e4c">
                                                        <div class="component-content">
                                                            <a href="https://www.hardrockcasinonorthernindiana.com/"
                                                               class="main-header-logo__link" title="">
                                                                <img
                                                                    src="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/logo-sticky.png?h=70&amp;iar=0&amp;w=123&amp;rev=590b409d2e6d4ab59fb9f48af0bd890f"
                                                                    alt="Hard Rock Casino Northern Indiana" width="123"
                                                                    height="70"/>

                                                                <span class="image-caption field-locator">Northern Indiana</span>

                                                            </a></div>
                                                    </div>


                                                    <div class="component smart-link-list main-header-logo__text"
                                                         data-uniqueid="smart_link_list-aa107f06f375438d99f43fc39cf5142a">
                                                        <div class="component-content">
                                                            <ul id="e48e876b260846ca977412db44274ded">
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockhotels.com/destinations.aspx"
                                                                               target="_blank" title="">Hard Rock
                                                                                Destinations</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="component smart-link-list main-header-top__right"
                                                         data-uniqueid="smart_link_list-3209695b7a3f4929bcf33e9ff8e4f233">
                                                        <div class="component-content">
                                                            <ul id="cce5043521d9453985c2a5571e49d961">
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/play-online"
                                                                               target="" title="">Play Online</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class="main-header-top__link--highlight "
                                                                               href="https://www.hardrockcasinonorthernindiana.com/unity"
                                                                               target="" title="Unity by Hard Rock">Unity
                                                                                by Hard
                                                                                Rock</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="component container-fluid drawer-container"
                                         data-uniqueid="container_fluid-4e3aa0963b5647e1802adb3525a76da2">
                                        <div class="component-content">
                                        </div>
                                    </div>


                                    <div class="component main-header-bottom"
                                         data-uniqueid="wrapper-9c46124a3d2a40eab03b84430276a7f0">
                                        <div class="component-content">


                                            <div class="component container main-header__inner"
                                                 data-uniqueid="container-7588d9de8fc14c05bdd8143c63d5b74b">
                                                <div class="component-content">


                                                    <div class="row component column-splitter">
                                                        <div
                                                            class="visible-xs-inline-block col-1 main-header-nav--mobile"
                                                            data-uniqueid="column_splitter-4be7d0fae72c4abd98586fd69bde2d35">
                                                            <div class="component plain-html main-header-nav__button"
                                                                 data-uniqueid="reusable_plain_html-ffa0957136eb41369e33a467ade5e45c">
                                                                <div class="component-content">
                                                                    <button type="button"
                                                                            class="main-header-nav__trigger">
                                                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2 col-5 main-header-logo"
                                                             data-uniqueid="column_splitter-4be7d0fae72c4abd98586fd69bde2d35">

                                                            <div
                                                                class="component logo-image file-type-icon-media-link image main-header-logo__image--desktop"
                                                                data-uniqueid="logo_image-a5e53e2058f74e02af80bb173f2f600f">
                                                                <div class="component-content">
                                                                    <a href="https://www.hardrockcasinonorthernindiana.com/"
                                                                       class="main-header-logo__link" title="">
                                                                        <img
                                                                            src="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/logo-desktop.png?h=84&amp;iar=0&amp;w=110&amp;rev=b84c46b725b045ec82140b86348d9874"
                                                                            alt="Hard Rock Casino Northern Indiana"
                                                                            width="110"
                                                                            height="84"/>

                                                                        <span
                                                                            class="image-caption field-locator"></span>

                                                                    </a></div>
                                                            </div>

                                                            <div
                                                                class="component logo-image file-type-icon-media-link image visible-xs-block main-header-logo__image--mobile"
                                                                data-uniqueid="logo_image-a445facb6f484df1aaf201a21e9ad82f">
                                                                <div class="component-content">
                                                                    <a href="https://www.hardrockcasinonorthernindiana.com/"
                                                                       class=""
                                                                       title="">
                                                                        <img
                                                                            src="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/logo-mobile.png?h=50&amp;iar=0&amp;w=87&amp;rev=078b496e124e43feb33884428525e8da"
                                                                            alt="Hard Rock Casino Northern Indiana"
                                                                            width="87"
                                                                            height="50"/>

                                                                        <span
                                                                            class="image-caption field-locator"></span>

                                                                    </a></div>
                                                            </div>

                                                        </div>
                                                        <div class="visible-xs-block col-6 main-header-wildcard"
                                                             data-uniqueid="column_splitter-4be7d0fae72c4abd98586fd69bde2d35">


                                                            <div class="component link"
                                                                 data-uniqueid="link-1597e11e6aa54cc492c178ae29854d6d">
                                                                <div class="component-content">


                                                                    <a class="toggle-drawer"
                                                                       href="https://www.hardrockcasinonorthernindiana.com/unity"
                                                                       target="" title="Unity by Hard Rock">Unity by
                                                                        Hard Rock</a>


                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="col-sm-12 col-md-10 hidden-xs col-12 main-navigation"
                                                            data-uniqueid="column_splitter-4be7d0fae72c4abd98586fd69bde2d35">


                                                            <div class="component smart-link-list"
                                                                 data-uniqueid="smart_link_list-5550dbec06cc4393b1cf66deabd15186">
                                                                <div class="component-content">
                                                                    <button type="button" class="main-header-nav__close"
                                                                            data-target="97477c1f1cce490a9d0d0660c9dd4952">

                                                                    </button>
                                                                    <ul class="main-navigation__list"
                                                                        id="97477c1f1cce490a9d0d0660c9dd4952">
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class="toggle-drawer"
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/unity"
                                                                                       target=""
                                                                                       title="Unity by Hard Rock">Unity
                                                                                        by
                                                                                        Hard Rock</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class="hide-desktop toggle-drawer"
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/play-online"
                                                                                       target="" title="">Play
                                                                                        Online</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class=""
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/casino"
                                                                                       target="" title="">Casino</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class=""
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/dining"
                                                                                       target="" title="">Dining</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class=""
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/bars-and-lounges"
                                                                                       target="" title="">Bars &
                                                                                        Lounges</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class=""
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/entertainment"
                                                                                       target=""
                                                                                       title="">Entertainment</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class=""
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/retail"
                                                                                       target="" title="">Retail</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>


                                                                            <div class="component link"
                                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                <div class="component-content">


                                                                                    <a class=""
                                                                                       href="https://www.hardrockcasinonorthernindiana.com/promotions"
                                                                                       target="" title="">Promotions</a>


                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        @if ((Auth::User()))
                                                                            <li>
                                                                                <div class="component link"
                                                                                     data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                    <div class="component-content">
                                                                                        <a class=""
                                                                                           href="#"
                                                                                           onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();"
                                                                                           target="" title="">Logout</a>
                                                                                        <form id="logout-form"
                                                                                              action="/logout"
                                                                                              method="POST"
                                                                                              style="display: none;">
                                                                                            @csrf
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        @else
                                                                            <li>
                                                                                <div class="component link"
                                                                                     data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                                    <div class="component-content">
                                                                                        <a class=""
                                                                                           href="/login"
                                                                                           target="" title="">Login</a>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                    </div>
                    <!-- /#header -->
                    <!-- #content -->
                    <div class="loader"></div>
                    <div id="content" class="content">
                        <!-- /#content -->
                        @yield('content')
                    </div>
                    <!-- #footer -->
                    <footer class="main-footer">
                        <div id="footer" class="container main-footer__inner">
                            <div class="row footerRow1">
                                <div class="component row-splitter" style="width: 100%;display: block !important;">
                                    <div class="container-fluid"
                                         data-uniqueid="row_splitter-e70478784ef641b29f27136cc5d767d4">
                                        <div class="row">
                                            <div class="row component column-splitter" style="width:100%">
                                                <div class="col-md-12 col-lg-4 main-footer-logo"
                                                     data-uniqueid="column_splitter-0fdd07ff9643450e8d2efcc4b5f141af">

                                                    <div
                                                        class="component logo-image file-type-icon-media-link image main-footer-logo__wrap"
                                                        data-uniqueid="logo_image-80dfb8e5ea454fcc9139035163038258">
                                                        <div class="component-content">
                                                            <a href="https://www.hardrockcasinonorthernindiana.com/"
                                                               class="" title="">
                                                                <img
                                                                    src="https://www.hardrockcasinonorthernindiana.com/-/media/project/shrss/hri/casinos/hard-rock/northern-indiana/logos-and-icons/logo-footer.png?h=284&amp;iar=0&amp;w=360&amp;rev=1b883c7241f84b21b67407b2757af8e4"
                                                                    alt="Hard Rock Casino Northern Indiana" width="360"
                                                                    height="284"/>

                                                                <span class="image-caption field-locator"></span>

                                                            </a></div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6 col-lg-4 main-footer-nav"
                                                     data-uniqueid="column_splitter-0fdd07ff9643450e8d2efcc4b5f141af">


                                                    <div class="component smart-link-list main-footer-nav__wrap"
                                                         data-uniqueid="smart_link_list-43c404f7f4964cfc80005e66b8a9ad41">
                                                        <div class="component-content">
                                                            <h5 style="color:white;">Hard Rock Casino</h5>
                                                            <ul id="f01ff295096c4a229e54dc2a117fc7e7">
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/contact-us"
                                                                               target="" title="">Contact Us</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/careers"
                                                                               target="" title="">Careers</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/social-responsibility"
                                                                               target="" title="">Social
                                                                                Responsibility</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/safe-and-sound"
                                                                               target="" title="">Safe+Sound</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/unity"
                                                                               target="" title="">Unity by Hard Rock</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=" external"
                                                                               href="https://hardrocksocialcasino.onelink.me/gyCq/e3b5eb1"
                                                                               target="_blank" title="">Play Online</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component link"
                                                                         data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                        <div class="component-content">


                                                                            <a class=""
                                                                               href="https://www.hardrockcasinonorthernindiana.com/press"
                                                                               target="" title="">Press</a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 main-footer-info"
                                                     data-uniqueid="column_splitter-0fdd07ff9643450e8d2efcc4b5f141af">


                                                    <div
                                                        class="component smart-link-list position-center main-footer-info__wrap"
                                                        data-uniqueid="smart_link_list-c57b6fffead2408e88edc8c188a507c0">
                                                        <div class="component-content">
                                                            <h5 style="color:white;">Find Us On</h5>
                                                            <ul id="ec24225b583347b0a3082bc3dabc8cca">
                                                                <li>


                                                                    <div class="component icon-link"
                                                                         data-uniqueid="icon_link-81f35b2f209a4b348201a3c21aa75b58">
                                                                        <div class="component-content">


                                                                            <a href="https://www.facebook.com/hrcasinonindiana/"
                                                                               class="" target="_blank">
                                                                                <i class=" fa fa-facebook "></i>
                                                                            </a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component icon-link"
                                                                         data-uniqueid="icon_link-81f35b2f209a4b348201a3c21aa75b58">
                                                                        <div class="component-content">


                                                                            <a href="https://twitter.com/HRCasinoIndiana"
                                                                               class=""
                                                                               target="_blank">
                                                                                <i class=" fa fa-twitter "></i>
                                                                            </a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component icon-link"
                                                                         data-uniqueid="icon_link-81f35b2f209a4b348201a3c21aa75b58">
                                                                        <div class="component-content">


                                                                            <a href="https://www.youtube.com/user/hardrock"
                                                                               class=""
                                                                               target="_blank">
                                                                                <i class=" fa fa-youtube "></i>
                                                                            </a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component icon-link"
                                                                         data-uniqueid="icon_link-81f35b2f209a4b348201a3c21aa75b58">
                                                                        <div class="component-content">


                                                                            <a href="https://www.instagram.com/hrcasinonindiana/"
                                                                               class="" target="_blank">
                                                                                <i class=" fa fa-instagram "></i>
                                                                            </a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>


                                                                    <div class="component icon-link"
                                                                         data-uniqueid="icon_link-81f35b2f209a4b348201a3c21aa75b58">
                                                                        <div class="component-content">


                                                                            <a href="https://www.linkedin.com/company/hard-rock-casino-northern-indiana/about/"
                                                                               class="" target="_blank">
                                                                                <i class=" fa fa-linkedin "></i>
                                                                            </a>


                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="component map-link position-center main-footer-address"
                                                         data-uniqueid="map_link-07847f9bf19445f1b3d8902e372b796b">
                                                        <div class="component-content">
                                                            <a href="https://goo.gl/maps/r2mHXmxVT96TK7C48" class=""
                                                               target="_blank">
                                                                5400 West 29th Avenue<br/>Gary, Indiana 46406<br/><br>
                                                            </a>

                                                        </div>
                                                    </div>

                                                    <div class="component telephone-link col-xs-12"
                                                         data-uniqueid="telephone_link-38a33b9b90e14e349194d732218eb87b">
                                                        <div class="component-content">


                                                            <a href="tel:2192282383">
                                                                (219) 228-2383
                                                            </a>

                                                        </div>
                                                    </div>

                                                    <div class="component link col-xs-12"
                                                         data-uniqueid="link-c9c53938535a49af8149f7cef719acf4">
                                                        <div class="component-content">


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" main-footer-copyright container-fluid"
                                         data-uniqueid="row_splitter-e70478784ef641b29f27136cc5d767d4">
                                        <div class="row">
                                            <div class="component rich-text col-xs-12"
                                                 data-uniqueid="reusablerichtext-96816c02c30741d689297316beeebc25">
                                                <div class="component-content">
                                                    Copyright &copy; 2021, Hard Rock Casino Northern Indiana. All Rights
                                                    Reserved.
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class=" main-footer-copyright__links container-fluid"
                                         data-uniqueid="row_splitter-e70478784ef641b29f27136cc5d767d4">
                                        <div class="row">


                                            <div class="component smart-link-list col-xs-12"
                                                 data-uniqueid="smart_link_list-8ed5a1cb978f41b6830ec4bd16c53c6c">
                                                <div class="component-content">
                                                    <ul id="b11f74c2286a405ab7533c959daa9b2a">
                                                        <li>


                                                            <div class="component link"
                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                <div class="component-content">


                                                                    <a class=""
                                                                       href="https://www.hardrockcasinonorthernindiana.com/responsible-gaming"
                                                                       target="" title="">Responsible Gaming</a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>


                                                            <div class="component link"
                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                <div class="component-content">


                                                                    <a class=" external" href="https://playersedge.org/"
                                                                       target="_blank"
                                                                       title="">PlayersEdge</a>


                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>


                                                            <div class="component link"
                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                <div class="component-content">


                                                                    <a class=""
                                                                       href="https://www.hardrockcasinonorthernindiana.com/privacy-policy"
                                                                       target="" title="">Privacy Policy</a>


                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>


                                                            <div class="component link"
                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                <div class="component-content">


                                                                    <a class=""
                                                                       href="https://www.hardrockcasinonorthernindiana.com/terms-of-use"
                                                                       target="" title="">Terms of Use</a>


                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>


                                                            <div class="component link"
                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                <div class="component-content">


                                                                    <a class=""
                                                                       href="https://www.hardrock.com/ccpa.aspx"
                                                                       target="_blank" title="">CCPA</a>


                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>


                                                            <div class="component link"
                                                                 data-uniqueid="link-5767b3fff3214037be2422ca71456ec5">
                                                                <div class="component-content">


                                                                    <a class="ot-sdk-show-settings"
                                                                       href="javascript:var b = document.querySelector('a');b.setAttribute('id', 'ot-sdk-btn');"
                                                                       target="" title="">Cookie Settings</a>


                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!-- /#footer -->
                </div>
                <!-- /#wrapper -->

                <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/mos-HardRockCinn.css">
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(window).load(function () {
        $(".loader").fadeOut("slow");
    });
</script>
</html>
