@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="text-align: center">
                    <div class="card-header">Verify Your Email Address</div>
                    <div class="card-body">
                        @if($data->status === true)
                            <p style="padding-bottom: 20px">{{ $data->message }}</p>
                            <a href="/admin/login" class="btn btn-block login-button"
                                    style="background-color: #59588c; border-color: #59588c; color: #fff;display: block; margin:0 auto;width: 140px">
                                Login
                            </a>
                        @else
                            <p style="color:red;padding-bottom: 20px">{{ $data->message }}</p>
                            <a href="https://www.hardrockcasinonorthernindiana.com/contact-us" class="btn btn-block login-button"
                                    style="background-color: #59588c; border-color: #59588c; color: #fff;display: block; margin:0 auto;;width: 140px">
                                Contact Us
                            </a>
                        @endif

                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
@endsection
