<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" style="background:red" href="{{ route('voyager.dashboard') }}">
                    <div class="logo-icon-container">
                        <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                        @if($admin_logo_img == '')
                            <img src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                        @else
                            <img src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                        @endif
                    </div>
                    <div class="title">{{Voyager::setting('admin.title', 'VOYAGER')}}</div>
                </a>
            </div><!-- .navbar-header -->

            <div class="panel widget center bgimage"
                 style="background-image:url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('images/bg.jpg') ) }}); background-size: cover; background-position: 0px;">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <img src="{{ $user_avatar }}" class="avatar" alt="{{ Auth::user()->name }} avatar">
                    <h4>{{ ucwords(Auth::user()->name) }}</h4>
                    <p>{{ Auth::user()->email }}</p>

                    <a href="{{ route('voyager.profile') }}"
                       class="btn btn-primary">{{ __('voyager::generic.profile') }}</a>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>
        <div id="adminmenu">
            @if ((Auth::User()))
                @if (Auth::user()->role_id === 4)
                    <admin-menu :items="{{ '
[
  {
    "id": 1,
    "menu_id": 1,
    "title": "Dashboard",
    "url": "",
    "target": "_self",
    "icon_class": "voyager-boat",
    "color": null,
    "parent_id": null,
    "order": 1,
    "created_at": "2021-03-18 22:56:07",
    "updated_at": "2021-03-18 22:56:07",
    "route": "voyager.dashboard",
    "parameters": null,
    "href": "https://dev.hardrockcasinonorthernindiana.maplewebservices.com/admin",
    "active": true,
    "children": []
  },
  {
    "id": 29,
    "menu_id": 1,
    "title": "Data",
    "url": "",
    "target": "_self",
    "icon_class": "voyager-list",
    "color": "#000000",
    "parent_id": null,
    "order": 2,
    "created_at": "2021-07-28 10:22:53",
    "updated_at": "2021-07-28 10:23:41",
    "route": "voyager.datas.index",
    "parameters": null,
    "href": "https://dev.hardrockcasinonorthernindiana.maplewebservices.com/admin/datas",
    "children": []
  },
  {
    "id": 30,
    "menu_id": 1,
    "title": "Reportings",
    "url": "",
    "target": "_self",
    "icon_class": "voyager-news",
    "color": "#000000",
    "parent_id": null,
    "order": 2,
    "created_at": "2021-07-28 10:22:53",
    "updated_at": "2021-07-28 10:23:41",
    "route": "voyager.reporting.index",
    "parameters": null,
    "href": "https://dev.hardrockcasinonorthernindiana.maplewebservices.com/admin/reporting",
    "children": []
  },
  {
    "id": 28,
    "menu_id": 1,
    "title": "Users",
    "url": "",
    "target": "_self",
    "icon_class": "voyager-people",
    "color": "#000000",
    "parent_id": null,
    "order": 3,
    "created_at": "2021-06-07 12:02:39",
    "updated_at": "2021-07-28 10:23:41",
    "route": "voyager.users.index",
    "parameters": null,
    "href": "https://dev.hardrockcasinonorthernindiana.maplewebservices.com/admin/users",
    "children": []
  }
]' }}"></admin-menu>
                @else
                    <admin-menu :items="{{ menu('admin', '_json') }}"></admin-menu>
                @endif
            @endif
        </div>
    </nav>
</div>
