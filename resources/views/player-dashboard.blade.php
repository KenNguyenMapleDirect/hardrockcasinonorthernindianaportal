@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center" style="padding-top: 20px;background: white">
            <div class="col-md-8 text-center">
                @if($data->flgCoreSM)
                    <div class="row">
                        <!-- novemberCoreSM 6 pages-->
                        @if($data->novemberCoreSM)
                            <div class="col-md-12">
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                                <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/lightbox-second.min.js')}}"></script>
                                <style>
                                    table {
                                        border-collapse: inherit !important;
                                    }

                                    .lb-details {
                                        display: none;
                                    }

                                    @font-face {
                                        font-family: myFirstFont;
                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                    }

                                    .firstname {
                                        font-family: myFirstFont;
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    @font-face {
                                        font-family: totalfb;
                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                    }

                                    .dollar-amount {
                                        font-family: 'totalfb';
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    /* .dollar-amount62 {
                                      font-family: 'Steelfish', sans-serif;
                                      font-weight: 700;
                                      font-size: 16pt;
                                    } */

                                    #img-magnifier-container-second {
                                        display: none;
                                        background: rgba(0, 0, 0, 0.8);
                                        border: 5px solid rgb(255, 255, 255);
                                        border-radius: 20px;
                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                        cursor: none;
                                        position: absolute;
                                        pointer-events: none;
                                        width: 400px;
                                        height: 200px;
                                        overflow: hidden;
                                        z-index: 999;
                                    }

                                    .glass {
                                        position: absolute;
                                        background-repeat: no-repeat;
                                        background-size: auto;
                                        cursor: none;
                                        z-index: 1000;
                                    }

                                    #toggle-zoom-second {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                        background-size: 40px;
                                        display: block;
                                        width: 40px;
                                        display: none;
                                        height: 40px;
                                    }

                                    #printer {
                                        float: right;
                                        display: block;
                                        width: 40px;
                                        height: 40px;
                                        margin-right: 20px;
                                        display: none;
                                    }

                                    #toggle-zoom-second.toggle-on {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                    }

                                    @media (hover: none) {
                                        .tool-zoom {
                                            display: none;
                                        }

                                        #printer {
                                            display: none;
                                        }
                                    }
                                </style>
                                <div class="flipbook-viewport-second">
                                    <div class="container">
                                        <div>
                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                               target="blank"><img
                                                    id="printer"
                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                        </div>
                                        <div class="tool-zoom">
                                            <a id="toggle-zoom-second" onclick="toggleZoom()"></a>
                                        </div>

                                        <div class="arrows">
                                            <div class="arrow-prev">
                                                <a id="prev-second"><img class="previous" width="20"
                                                                         src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                         alt=""/></a>
                                            </div>
                                            <div style="text-align: center;"><h4
                                                    style="color: #ddbb30;font-size:25px;padding-bottom:20px">
                                                    November Core Self Mailer</h4></div>
                                            <div class="flipbook-second">

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult1}}"
                                                   data-odd="1"
                                                   id="page-1"
                                                   data-lightbox-second="big" class="page"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult1}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult2}}"
                                                   data-even="2"
                                                   id="page-2" data-lightbox-second="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult2}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult3}}"
                                                   data-odd="3"
                                                   id="page-3"
                                                   data-lightbox-second="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult3}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult4}}"
                                                   data-even="4"
                                                   id="page-4" data-lightbox-second="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult4}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult5}}"
                                                   data-odd="5"
                                                   id="page-5"
                                                   data-lightbox-second="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult5}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult6}}"
                                                   data-even="6"
                                                   id="page-6" data-lightbox-second="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/lo_res/{{$data->novemberCoreSMResult6}}')"></a>

{{--                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult7}}"--}}
{{--                                                   data-even="7"--}}
{{--                                                   id="page-7" data-lightbox-second="big" class="single"--}}
{{--                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/lo_res/{{$data->novemberCoreSMResult7}}')"></a>--}}

{{--                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult8}}"--}}
{{--                                                   data-even="8"--}}
{{--                                                   id="page-8" data-lightbox-second="big" class="single"--}}
{{--                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/lo_res/{{$data->novemberCoreSMResult8}}')"></a>--}}
                                            </div>
                                            <div class="arrow-next">
                                                <a id="next-second"><img class="next" width="20"
                                                                         src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                         alt=""/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flipbook-slider-thumb-second">
                                    <div class="drag-second">
                                        <!-- <img id="prev-arrow-second" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                        <img onclick="onPageClickSecond(1)" class="thumb-img left-img"
                                             src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult1}}"
                                             alt=""/>

                                        <div class="space">
                                            <img onclick="onPageClickSecond(2)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult2}}"
                                                 alt=""/>
                                            <img onclick="onPageClickSecond(3)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult3}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClickSecond(4)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult4}}"
                                                 alt=""/>
                                            <img onclick="onPageClickSecond(5)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult5}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClickSecond(6)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult6}}"
                                                 alt=""/>
{{--                                            <img onclick="onPageClickSecond(7)" class="thumb-img"--}}
{{--                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/hi_res/{{$data->novemberCoreSMResult7}}"--}}
{{--                                                 alt=""/>--}}
                                        </div>

{{--                                        <div class="space">--}}
{{--                                            <img onclick="onPageClickSecond(8)" class="thumb-img active"--}}
{{--                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/november_2021/lo_res/{{$data->novemberCoreSMResult8}}"--}}
{{--                                                 alt=""/>--}}
{{--                                        </div>--}}

                                    <!-- <img id="next-arrow-second" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                                    </div>

                                    <ul class="flipbook-slick-dots-second" role="tablist">
                                        <li onclick="onPageClickSecond(1)" class="dot">
                                            <a type="button" style="color: #7f7f7f">1</a>
                                        </li>
                                        <li onclick="onPageClickSecond(2)" class="dot">
                                            <a type="button" style="color: #7f7f7f">2</a>
                                        </li>
                                        <li onclick="onPageClickSecond(3)" class="dot">
                                            <a type="button" style="color: #7f7f7f">3</a>
                                        </li>
                                        <li onclick="onPageClickSecond(4)" class="dot">
                                            <a type="button" style="color: #7f7f7f">4</a>
                                        </li>
                                        <li onclick="onPageClickSecond(5)" class="dot">
                                            <a type="button" style="color: #7f7f7f">5</a>
                                        </li>
                                        <li onclick="onPageClickSecond(6)" class="dot">
                                            <a type="button" style="color: #7f7f7f">6</a>
                                        </li>
{{--                                        <li onclick="onPageClickSecond(7)" class="dot">--}}
{{--                                            <a type="button" style="color: #7f7f7f">7</a>--}}
{{--                                        </li>--}}
{{--                                        <li onclick="onPageClickSecond(8)" class="dot">--}}
{{--                                            <a type="button" style="color: #7f7f7f">8</a>--}}
{{--                                        </li>--}}
                                    </ul>
                                </div>
                                <div id="img-magnifier-container-second">
                                    <img id="zoomed-image-container" class="glass" src=""/>
                                </div>
                                <div id="log"></div>
                                <audio id="audio" style="display: none"
                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                <script type="text/javascript">
                                    function scaleFlipBookSecond() {
                                        var imageWidth = 541;
                                        var imageHeight = 850;
                                        var pageHeight = 550;
                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);
                                        $(".flipbook-viewport-second .container").css({
                                            width: 40 + pageWidth * 2 + 40 + "px",
                                        });

                                        $(".flipbook-viewport-second .flipbook-second").css({
                                            width: pageWidth * 2 + "px",
                                            height: pageHeight + "px",
                                        });
                                    }

                                    function doResizeSecond() {
                                        $("html").css({
                                            zoom: 1
                                        });
                                        var $viewportSecond = $(".flipbook-viewport-second");
                                        var viewHeight = $viewportSecond.height();
                                        var viewWidth = $viewportSecond.width();

                                        var $el = $(".flipbook-viewport-second .container");
                                        var elHeight = $el.outerHeight();
                                        var elWidth = $el.outerWidth();

                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                        if (scale < 1) {
                                            scale *= 0.95;
                                        } else {
                                            scale = 1;
                                        }
                                        // $("html").css({
                                        //     zoom: scale
                                        // });
                                    }

                                    function loadAppSecond() {
                                        scaleFlipBookSecond();
                                        var flipbookSecond = $(".flipbook-second");

                                        // Check if the CSS was already loaded

                                        if (flipbookSecond.width() == 0 || flipbookSecond.height() == 0) {
                                            setTimeout(loadAppSecond, 10);
                                            return;
                                        }

                                        $(".flipbook-second .double").scissor();

                                        // Create the flipbook-second

                                        $(".flipbook-second").turn({
                                            // Elevation

                                            elevation: 50,

                                            // Enable gradients

                                            gradients: true,

                                            // Auto center this flipbook-second

                                            autoCenter: true,
                                            when: {
                                                turning: function (event, page, view) {
                                                    var audio = document.getElementById("audio");
                                                    audio.play();
                                                },
                                                turned: function (e, page) {
                                                    //console.log('Current view: ', $(this).turn('view'));
                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                    for (var i = 0; i < thumbs.length; i++) {
                                                        var element = thumbs[i];
                                                        if (element.className.indexOf("active") !== -1) {
                                                            $(element).removeClass("active");
                                                        }
                                                    }

                                                    $(
                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                    ).addClass("active");

                                                    var dots = document.getElementsByClassName("dot");
                                                    for (var i = 0; i < dots.length; i++) {
                                                        var dot = dots[i];
                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                            $(dot).removeClass("dot-active");
                                                        }
                                                    }
                                                },
                                            },
                                        });
                                        doResizeSecond();
                                    }

                                    $(window).resize(function () {
                                        doResizeSecond();
                                    });
                                    $(window).bind("keydown", function (e) {
                                        if (e.keyCode == 37) $(".flipbook-second").turn("previous");
                                        else if (e.keyCode == 39) $(".flipbook-second").turn("next");
                                    });
                                    $("#prev-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("previous");
                                    });

                                    $("#next-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("next");
                                    });

                                    $("#prev-arrow-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("previous");
                                    });

                                    $("#next-arrow-second").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook-second").turn("next");
                                    });

                                    function onPageClickSecond(i) {
                                        $(".flipbook-second").turn("page", i);
                                    }

                                    // Load the HTML4 version if there's not CSS transform
                                    yepnope({
                                        test: Modernizr.csstransforms,
                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page-second.css')}}"],
                                        complete: loadAppSecond,
                                    });

                                    zoomToolEnabled = false;

                                    function toggleZoom() {
                                        if (zoomToolEnabled) {
                                            $(".flipbook-second a").off("mousemove");
                                            $("#toggle-zoom-second").removeClass("toggle-on");
                                            $("#img-magnifier-container-second").hide();

                                            zoomToolEnabled = false;
                                        } else {
                                            $(".flipbook-second a").mousemove(function (event) {
                                                var magnifier = $("#img-magnifier-container-second");
                                                $("#img-magnifier-container-second").css(
                                                    "left",
                                                    event.pageX - magnifier.width() / 2
                                                );
                                                $("#img-magnifier-container-second").css(
                                                    "top",
                                                    event.pageY - magnifier.height() / 2
                                                );
                                                $("#img-magnifier-container-second").show();
                                                var hoveredImage = $(event.target).css("background-image");
                                                var bg = hoveredImage
                                                    .replace("url(", "")
                                                    .replace(")", "")
                                                    .replace(/\"/gi, "");
                                                // Find relative position of cursor in image.
                                                var targetPage = $(event.target);
                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                var targetTop = 200 / 2; // Height of glass container/2

                                                var zoomedImageContainer = document.getElementById(
                                                    "zoomed-image-container"
                                                );
                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                var imgXPercent =
                                                    (event.pageX - $(event.target).offset().left) /
                                                    targetPage.width();
                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                var imgYPercent =
                                                    (event.pageY - $(event.target).offset().top) /
                                                    targetPage.height();
                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                $("#img-magnifier-container-second .glass").attr("src", bg);
                                                $("#img-magnifier-container-second .glass").css(
                                                    "top",
                                                    "" + targetTop + "px"
                                                );
                                                $("#img-magnifier-container-second .glass").css(
                                                    "left",
                                                    "" + targetLeft + "px"
                                                );
                                            });

                                            $("#toggle-zoom-second").addClass("toggle-on");
                                            zoomToolEnabled = true;
                                        }
                                    }
                                </script>
                            </div>
                        @endif
                        <!-- octoberCoreSM 8 pages-->
                    @if($data->octoberCoreSM)
                            <div class="col-md-12">
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/modernizr.2.5.3.min.js')}}"></script>
                                <link rel="stylesheet" href="{{asset('flipbook_assets/css/lightbox.min.css')}}"/>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/dynamic-content.js')}}"></script>
                                <script type="text/javascript"
                                        src="{{asset('flipbook_assets/js/lightbox.min.js')}}"></script>
                                <style>
                                    table {
                                        border-collapse: inherit !important;
                                    }

                                    .lb-details {
                                        display: none;
                                    }

                                    @font-face {
                                        font-family: myFirstFont;
                                        src: url({{asset('flipbook_assets/ProximaNova-Bold.otf')}}) format('opentype');
                                    }

                                    .firstname {
                                        font-family: myFirstFont;
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    @font-face {
                                        font-family: totalfb;
                                        src: url({{asset('flipbook_assets/Steelfish-Bold.ttf')}}) format('truetype');

                                    }

                                    .dollar-amount {
                                        font-family: 'totalfb';
                                        font-weight: 700;
                                        font-size: 16pt;
                                    }

                                    /* .dollar-amount62 {
                                      font-family: 'Steelfish', sans-serif;
                                      font-weight: 700;
                                      font-size: 16pt;
                                    } */

                                    #img-magnifier-container {
                                        display: none;
                                        background: rgba(0, 0, 0, 0.8);
                                        border: 5px solid rgb(255, 255, 255);
                                        border-radius: 20px;
                                        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85),
                                        0 0 7px 7px rgba(0, 0, 0, 0.25),
                                        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);
                                        cursor: none;
                                        position: absolute;
                                        pointer-events: none;
                                        width: 400px;
                                        height: 200px;
                                        overflow: hidden;
                                        z-index: 999;
                                    }

                                    .glass {
                                        position: absolute;
                                        background-repeat: no-repeat;
                                        background-size: auto;
                                        cursor: none;
                                        z-index: 1000;
                                    }

                                    #toggle-zoom {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLACK.png')}}");
                                        background-size: 40px;
                                        display: block;
                                        width: 40px;
                                        display: none;
                                        height: 40px;
                                    }

                                    #printer {
                                        float: right;
                                        display: block;
                                        width: 40px;
                                        height: 40px;
                                        margin-right: 20px;
                                        display: none;
                                    }

                                    #toggle-zoom.toggle-on {
                                        background-image: url("{{asset('flipbook_assets/images/magnifying-glass-large_BLUE.png')}}");
                                    }

                                    @media (hover: none) {
                                        .tool-zoom {
                                            display: none;
                                        }

                                        #printer {
                                            display: none;
                                        }
                                    }
                                </style>
                                <div class="flipbook-viewport">
                                    <div class="container">
                                        <div>
                                            <a href={{asset('flipbook_assets/pages/Booklet.pdf')}}""
                                               target="blank"><img
                                                    id="printer"
                                                    src="{{asset('flipbook_assets/pages/printer-large.png')}}"/></a>
                                        </div>
                                        <div class="tool-zoom">
                                            <a id="toggle-zoom" onclick="toggleZoom()"></a>
                                        </div>

                                        <div class="arrows">
                                            <div class="arrow-prev">
                                                <a id="prev"><img class="previous" width="20"
                                                                  src="{{asset('flipbook_assets/pages/left-arrow.svg')}}"
                                                                  alt=""/></a>
                                            </div>
                                            <center><h4 style="color: #ddbb30;font-size:25px;padding-bottom:20px">
                                                    October Core Self Mailer</h4></center>
                                            <div class="flipbook">

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult1}}"
                                                   data-odd="1"
                                                   id="page-1"
                                                   data-lightbox="big" class="page"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult1}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult2}}"
                                                   data-even="2"
                                                   id="page-2" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult2}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult3}}"
                                                   data-odd="3"
                                                   id="page-3"
                                                   data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult3}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult4}}"
                                                   data-even="4"
                                                   id="page-4" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult4}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult5}}"
                                                   data-odd="5"
                                                   id="page-5"
                                                   data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult5}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult6}}"
                                                   data-even="6"
                                                   id="page-6" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/lo_res/{{$data->octoberCoreSMResult6}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult7}}"
                                                   data-even="7"
                                                   id="page-7" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/lo_res/{{$data->octoberCoreSMResult7}}')"></a>

                                                <a href="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult8}}"
                                                   data-even="8"
                                                   id="page-8" data-lightbox="big" class="single"
                                                   style="background-image: url('https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/lo_res/{{$data->octoberCoreSMResult8}}')"></a>
                                            </div>
                                            <div class="arrow-next">
                                                <a id="next"><img class="next" width="20"
                                                                  src="{{asset('flipbook_assets/pages/right-arrow.svg')}}"
                                                                  alt=""/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flipbook-slider-thumb">
                                    <div class="drag">
                                        <!-- <img id="prev-arrow" class="thumb-arrow" src="assets/pages/left-arrow.svg" alt=""> -->
                                        <img onclick="onPageClick(1)" class="thumb-img left-img"
                                             src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult1}}"
                                             alt=""/>

                                        <div class="space">
                                            <img onclick="onPageClick(2)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult2}}"
                                                 alt=""/>
                                            <img onclick="onPageClick(3)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult3}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClick(4)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult4}}"
                                                 alt=""/>
                                            <img onclick="onPageClick(5)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult5}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClick(6)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult6}}"
                                                 alt=""/>
                                            <img onclick="onPageClick(7)" class="thumb-img"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/hi_res/{{$data->octoberCoreSMResult7}}"
                                                 alt=""/>
                                        </div>

                                        <div class="space">
                                            <img onclick="onPageClick(8)" class="thumb-img active"
                                                 src="https://s3.amazonaws.com/hardrockcasinonorthernindiana.maplewebservices.com/october_2021/lo_res/{{$data->octoberCoreSMResult8}}"
                                                 alt=""/>
                                        </div>

                                    <!-- <img id="next-arrow" class="thumb-arrow" src="{{asset('flipbook_assets/pages/right-arrow.svg')}}" alt=""> -->
                                    </div>

                                    <ul class="flipbook-slick-dots" role="tablist">
                                        <li onclick="onPageClick(1)" class="dot">
                                            <a type="button" style="color: #7f7f7f">1</a>
                                        </li>
                                        <li onclick="onPageClick(2)" class="dot">
                                            <a type="button" style="color: #7f7f7f">2</a>
                                        </li>
                                        <li onclick="onPageClick(3)" class="dot">
                                            <a type="button" style="color: #7f7f7f">3</a>
                                        </li>
                                        <li onclick="onPageClick(4)" class="dot">
                                            <a type="button" style="color: #7f7f7f">4</a>
                                        </li>
                                        <li onclick="onPageClick(5)" class="dot">
                                            <a type="button" style="color: #7f7f7f">5</a>
                                        </li>
                                        <li onclick="onPageClick(6)" class="dot">
                                            <a type="button" style="color: #7f7f7f">6</a>
                                        </li>
                                        <li onclick="onPageClick(7)" class="dot">
                                            <a type="button" style="color: #7f7f7f">7</a>
                                        </li>
                                        <li onclick="onPageClick(8)" class="dot">
                                            <a type="button" style="color: #7f7f7f">8</a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="img-magnifier-container">
                                    <img id="zoomed-image-container" class="glass" src=""/>
                                </div>
                                <div id="log"></div>
                                <audio id="audio" style="display: none"
                                       src="{{asset('flipbook_assets/page-flip.mp3')}}"></audio>
                                <script type="text/javascript">
                                    function scaleFlipBook() {
                                        var imageWidth = 541;
                                        var imageHeight = 850;
                                        var pageHeight = 550;
                                        var pageWidth = parseInt((pageHeight / imageHeight) * imageWidth);
                                        $(".flipbook-viewport .container").css({
                                            width: 40 + pageWidth * 2 + 40 + "px",
                                        });

                                        $(".flipbook-viewport .flipbook").css({
                                            width: pageWidth * 2 + "px",
                                            height: pageHeight + "px",
                                        });
                                    }

                                    function doResize() {
                                        $("html").css({
                                            zoom: 1
                                        });
                                        var $viewport = $(".flipbook-viewport");
                                        var viewHeight = $viewport.height();
                                        var viewWidth = $viewport.width();

                                        var $el = $(".flipbook-viewport .container");
                                        var elHeight = $el.outerHeight();
                                        var elWidth = $el.outerWidth();

                                        var scale = Math.min(viewWidth / elWidth, viewHeight / elHeight);
                                        //console.log(viewWidth , elWidth, viewHeight , elHeight, scale);
                                        if (scale < 1) {
                                            scale *= 0.95;
                                        } else {
                                            scale = 1;
                                        }
                                        // $("html").css({
                                        //     zoom: scale
                                        // });
                                    }

                                    function loadApp() {
                                        scaleFlipBook();
                                        var flipbook = $(".flipbook");

                                        // Check if the CSS was already loaded

                                        if (flipbook.width() == 0 || flipbook.height() == 0) {
                                            setTimeout(loadApp, 10);
                                            return;
                                        }

                                        $(".flipbook .double").scissor();

                                        // Create the flipbook

                                        $(".flipbook").turn({
                                            // Elevation

                                            elevation: 50,

                                            // Enable gradients

                                            gradients: true,

                                            // Auto center this flipbook

                                            autoCenter: true,
                                            when: {
                                                turning: function (event, page, view) {
                                                    var audio = document.getElementById("audio");
                                                    audio.play();
                                                },
                                                turned: function (e, page) {
                                                    //console.log('Current view: ', $(this).turn('view'));
                                                    var thumbs = document.getElementsByClassName("thumb-img");
                                                    for (var i = 0; i < thumbs.length; i++) {
                                                        var element = thumbs[i];
                                                        if (element.className.indexOf("active") !== -1) {
                                                            $(element).removeClass("active");
                                                        }
                                                    }

                                                    $(
                                                        document.getElementsByClassName("thumb-img")[page - 1]
                                                    ).addClass("active");

                                                    var dots = document.getElementsByClassName("dot");
                                                    for (var i = 0; i < dots.length; i++) {
                                                        var dot = dots[i];
                                                        if (dot.className.indexOf("dot-active") !== -1) {
                                                            $(dot).removeClass("dot-active");
                                                        }
                                                    }
                                                },
                                            },
                                        });
                                        doResize();
                                    }

                                    $(window).resize(function () {
                                        doResize();
                                    });
                                    $(window).bind("keydown", function (e) {
                                        if (e.keyCode == 37) $(".flipbook").turn("previous");
                                        else if (e.keyCode == 39) $(".flipbook").turn("next");
                                    });
                                    $("#prev").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("previous");
                                    });

                                    $("#next").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("next");
                                    });

                                    $("#prev-arrow").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("previous");
                                    });

                                    $("#next-arrow").click(function (e) {
                                        e.preventDefault();
                                        $(".flipbook").turn("next");
                                    });

                                    function onPageClick(i) {
                                        $(".flipbook").turn("page", i);
                                    }

                                    // Load the HTML4 version if there's not CSS transform
                                    yepnope({
                                        test: Modernizr.csstransforms,
                                        yep: ["{{asset('flipbook_assets/js/turn.min.js')}}"],
                                        nope: ["{{asset('flipbook_assets/js/turn.html4.min.js')}}"],
                                        both: ["{{asset('flipbook_assets/js/scissor.min.js')}}", "{{asset('flipbook_assets/css/double-page.css')}}"],
                                        complete: loadApp,
                                    });

                                    zoomToolEnabled = false;

                                    function toggleZoom() {
                                        if (zoomToolEnabled) {
                                            $(".flipbook a").off("mousemove");
                                            $("#toggle-zoom").removeClass("toggle-on");
                                            $("#img-magnifier-container").hide();

                                            zoomToolEnabled = false;
                                        } else {
                                            $(".flipbook a").mousemove(function (event) {
                                                var magnifier = $("#img-magnifier-container");
                                                $("#img-magnifier-container").css(
                                                    "left",
                                                    event.pageX - magnifier.width() / 2
                                                );
                                                $("#img-magnifier-container").css(
                                                    "top",
                                                    event.pageY - magnifier.height() / 2
                                                );
                                                $("#img-magnifier-container").show();
                                                var hoveredImage = $(event.target).css("background-image");
                                                var bg = hoveredImage
                                                    .replace("url(", "")
                                                    .replace(")", "")
                                                    .replace(/\"/gi, "");
                                                // Find relative position of cursor in image.
                                                var targetPage = $(event.target);
                                                var targetLeft = 400 / 2; // Width of glass container/2
                                                var targetTop = 200 / 2; // Height of glass container/2

                                                var zoomedImageContainer = document.getElementById(
                                                    "zoomed-image-container"
                                                );
                                                var zoomedImageWidth = zoomedImageContainer.width;
                                                var zoomedImageHeight = zoomedImageContainer.height;

                                                var imgXPercent =
                                                    (event.pageX - $(event.target).offset().left) /
                                                    targetPage.width();
                                                targetLeft -= zoomedImageWidth * imgXPercent;
                                                var imgYPercent =
                                                    (event.pageY - $(event.target).offset().top) /
                                                    targetPage.height();
                                                targetTop -= zoomedImageHeight * imgYPercent;

                                                $("#img-magnifier-container .glass").attr("src", bg);
                                                $("#img-magnifier-container .glass").css(
                                                    "top",
                                                    "" + targetTop + "px"
                                                );
                                                $("#img-magnifier-container .glass").css(
                                                    "left",
                                                    "" + targetLeft + "px"
                                                );
                                            });

                                            $("#toggle-zoom").addClass("toggle-on");
                                            zoomToolEnabled = true;
                                        }
                                    }
                                </script>
                            </div>
                        @endif
                    </div>
                @elseif(!$data->flgBB)
                    <span>Sorry there are no offers to view<br>Come back and visit soon to qualify for future offers!</span>
                    @endif
                    </br>
                    <!-- Email BB area -->
                    @if($data->flgBB)
                        <div style="text-align: center;"><h4 style="color: #ddbb30;font-size:25px;padding-bottom:20px">
                                Hard Rock Casino of Northern Indiana Email Campaign</h4></div>
                        <div class="row justify-content-center row-cols-5" style="padding-top: 20px;background: white">
                            @foreach($data->octoberBB as $octoberBB)
                            @if(strpos($octoberBB->NIN_Label,'Weekly') !== false)
                                    <div class="sm-6 md-1-5 lg-1-5" style="padding:5px">
                                    <body>
                                        <div class="modal-wrap">
                                            <div class="slider-big">
                                                <a href="{{$octoberBB->imgLink}}"
                                                   data-lightbox-bb1="roadtrip"
                                                   style="text-align: center">
                                                    <img
                                                        src="{{$octoberBB->imgLink}}"
                                                        style="width: 100%;margin: 0 auto;"
                                                        alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <script
                                            src="/weekender_assets/js/modal_lightbox_bb_1.js"></script>
                                        </body>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <div class="row justify-content-center row-cols-5" style="padding-top: 20px;background: white">
                            @foreach($data->octoberBB as $octoberBB)
                                @if(strpos($octoberBB->NIN_Label,'Daily')!== false && strpos($octoberBB->NIN_Label,'Slot')=== false)
                                    <div class="sm-6 md-1-5 lg-1-5" style="padding:5px">
                                        <body>
                                        <div class="modal-wrap">
                                            <div class="slider-big">
                                                <a href="{{$octoberBB->imgLink}}"
                                                   data-lightbox-bb1="roadtrip"
                                                   style="text-align: center">
                                                    <img
                                                        src="{{$octoberBB->imgLink}}"
                                                        style="width: 100%;margin: 0 auto;"
                                                        alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <script
                                            src="/weekender_assets/js/modal_lightbox_bb_1.js"></script>
                                        </body>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <div class="row justify-content-center row-cols-5" style="padding-top: 20px;background: white">
                            @foreach($data->octoberBB as $octoberBB)
                                @if(strpos($octoberBB->NIN_Label,'Slot')!== false)
                                    <div class="sm-6 md-1-5 lg-1-5" style="padding:5px">
                                        <body>
                                        <div class="modal-wrap">
                                            <div class="slider-big">
                                                <a href="{{$octoberBB->imgLink}}"
                                                   data-lightbox-bb1="roadtrip"
                                                   style="text-align: center">
                                                    <img
                                                        src="{{$octoberBB->imgLink}}"
                                                        style="width: 100%;margin: 0 auto;"
                                                        alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <script
                                            src="/weekender_assets/js/modal_lightbox_bb_1.js"></script>
                                        </body>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <br>
                    @endif
            </div>
            <div class="col-md-4">
                <div id="mosCenterRight" style="">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>Promotions</title>
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/mos.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/promosBoxesBBack.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/slick.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/slick-mos.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/slick-theme.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/slick-theme-mos.css">

                    <!-- mosContent end-->
                    <script type="text/javascript" src="/HeaderFooterAssets/js/slick.js" charset="utf-8"></script>
                    <script type="text/javascript" src="/HeaderFooterAssets/js/mresize.js"></script>

                    <script type="text/javascript">
                        (function ($) {
                            'use strict';

                            $(function () {

                                $(document).on('ready', function () {

                                    var slideCnt = 0;
                                    var slidesToShow = 0;

                                    var isMobile = false; //initiate as false
                                    // device detection
                                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                                        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
                                        isMobile = true;
                                    }

                                    $('.promoStuffBBack').on('init', function (event, slick) {
                                        //alert('slider was initialized');
                                        slideCnt = slick.slideCount;
                                    });

                                    $('.promoStuffBBack').on('reInit', function (event, slick) {
                                        //alert('slider was re-initialized');
                                    });

                                    $('.promoStuffBBack').slick({
                                        infinite: false,
                                        //appendDots: $('.pdots'),
                                        slidesToShow: 2,
                                        slidesToScroll: 2,
                                        rows: 1,
                                        //variableWidth: true,
                                        dots: true,
                                        arrows: true,
                                        prevArrow: '<div class="slick-prev"></div>',
                                        nextArrow: '<div class="slick-next"></div>'
                                    });

                                    $("#promoGridBoxesBBack").on("mresize", function () {

                                        //alert('#promoGridBoxes Resized');
                                        //alert($('#promoGridBoxes').width() );

                                        var iBoxWidth = $('#promoGridBoxesBBack').width();

                                        //if ( iBoxWidth <= 500 && isMobile ) {
                                        if (iBoxWidth < 400 && isMobile) {
                                            //alert("Under 500");
                                            //alert("Is Mobile Kicked in");

                                            $('.promoStuffBBack').slick('unslick');

                                            $('.promoStuffBBack').slick({
                                                infinite: false,
                                                //appendDots: $('.pdots'),
                                                slidesToShow: 1,
                                                slidesToScroll: 1,
                                                rows: 1,
                                                //variableWidth: true,
                                                dots: true
                                            });

                                        } else if (iBoxWidth <= 640) {

                                            $('.promoStuffBBack').slick('unslick');

                                            $('.promoStuffBBack').slick({
                                                infinite: false,
                                                //appendDots: $('.pdots'),
                                                slidesToShow: 2,
                                                slidesToScroll: 1,
                                                rows: 1,
                                                slidesPerRow: 1,
                                                //variableWidth: true,
                                                dots: true
                                            });

                                        } else if (iBoxWidth <= 800) {

                                            $('.promoStuffBBack').slick('unslick');

                                            $('.promoStuffBBack').slick({
                                                infinite: false,
                                                //appendDots: $('.pdots'),
                                                slidesToShow: 3,
                                                slidesToScroll: 1,
                                                rows: 1,
                                                slidesPerRow: 1,
                                                //variableWidth: true,
                                                dots: true,
                                                arrows: true,
                                                prevArrow: '<div class="slick-prev"></div>',
                                                nextArrow: '<div class="slick-next"></div>'
                                            });
                                        } else if (iBoxWidth <= 950) {

                                            $('.promoStuffBBack').slick('unslick');

                                            $('.promoStuffBBack').slick({
                                                infinite: false,
                                                //appendDots: $('.pdots'),
                                                slidesToShow: 3,
                                                slidesToScroll: 1,
                                                rows: 1,
                                                slidesPerRow: 1,
                                                //variableWidth: true,
                                                dots: true,
                                                arrows: true,
                                                prevArrow: '<div class="slick-prev"></div>',
                                                nextArrow: '<div class="slick-next"></div>'
                                            });
                                        }

                                        //slideCnt = slideCnt + 2;
                                        //alert("slick slide cnt=" + slideCnt);

                                        slidesToShow = $('.promoStuffBBack').slick('slickGetOption', 'slidesToShow');
                                        //alert("slick slides to show=" + slidesToShow);

                                        if (slideCnt <= slidesToShow) {
                                            $("#promoGridBoxWrapperBBack ul").remove();
                                        }
                                    });


                                    // Slick Slider Text/Image delay fix
                                    $(window).load(function () {
                                        //alert("loaded");
                                        $('.promoGridBoxTextDisplayBBack').fadeIn();
                                        $('.promoGridBoxTextDisplayBBack').css('display', 'block');
                                    });
                                });
                            });

                        })(jQuery);
                    </script>

                    <div id="HideShow" style="clear:both;">

                        <script type="text/javascript">(function ($) {
                                'use strict';
                                $(function () {
                                    $(document).on('ready', function () {
                                        $('#promosBBack').hide();
                                    })
                                });
                            })(jQuery);</script>
                    </div>
                    <meta name="viewport" content="width=device-width, initial-scale=1">

                    <div id="eStatementContainer">
                        <!--  Start eStatement  -->
                        <div id="eStatementContainerWrapper">

                            <div id="eStatmentTitle">
                                <div>
                                    <h2>Card Level</h2>
                                </div>
                            </div>

                            <div id="eStatementContainerInner">

                                <div id="welcome">Hi, {{$data->first_name}} {{$data->last_name}}</div>

                                <div id="eStatement">

                                    <div id="membershipSection">
                                        <div id="yourMembershipLevelImageSection">
                                            @switch($data->currentTier)
                                                @case('Icon')
                                                <img id="yourMembershipLevelImage"
                                                     src="/HeaderFooterAssets/images/HR_Unity_Card_Icon-wShadow.png"
                                                     alt="Wildcard Elite Card">
                                                @break
                                                @case('Legend')
                                                <img id="yourMembershipLevelImage"
                                                     src="/HeaderFooterAssets/images/HR_Unity_Card_Legend-wShadow.png"
                                                     alt="Wildcard Elite Card">
                                                @break
                                                @case('XCARD')
                                                <img id="yourMembershipLevelImage"
                                                     src="/HeaderFooterAssets/images/HR_Unity_Card_X-wShadow.png"
                                                     alt="Wildcard Elite Card">
                                                @break
                                                @default
                                                <img id="yourMembershipLevelImage"
                                                     src="/HeaderFooterAssets/images/HR_Unity_Card_Star-wShadow.png"
                                                     alt="Wildcard Elite Card">
                                            @endswitch


                                        </div>
                                        <div id="yourMembershipLevel">
                                            <div style="float: left;  width: 160px; text-align: left; font-size: 14px;">
                                                Account number:
                                            </div>
                                            <div
                                                style="float: left; color: red; font-size: 14px;">{{$data->NIN_Account}}</div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        <div id="yourMembershipLevel">
                                            <div style="float: left;  width: 160px; text-align: left; font-size: 14px;">
                                                Tier Credits:
                                            </div>
                                            <div
                                                style="float: left; color: black; font-size: 14px;">{{$data->tierCredits}}</div>
                                            <div style="clear:both;"></div>
                                        </div>
                                        <div id="yourMembershipLevel">
                                            <div style="float: left;  width: 160px; text-align: left; font-size: 14px;">
                                                Unity Points:
                                            </div>
                                            <div
                                                style="float: left; color: black; font-size: 14px;"> {{$data->comp_Dollar_Balance}}</div>
                                            <div style="clear:both;"></div>
                                        </div>

                                        <div id="yourMembershipLevel">
                                            <div style="float: left;  width: 160px; text-align: left; font-size: 14px;">
                                                &nbsp;
                                            </div>
                                            <div style="float: left; color: black; font-size: 14px;">&nbsp;</div>
                                            <div style="clear:both;"></div>
                                        </div>

                                        <div id="yourMembershipLevel" class="yourMembershipLevelAttrib">
                                            <div style="float: left; width: 160px; text-align: left; font-size: 14px;">
                                                Card Level:
                                            </div>
                                            <div
                                                style="float: left; color: black; font-size: 14px;">{{$data->currentTier}}</div>
                                            <div style="clear:both;"></div>
                                        </div>

                                        <div id="yourMembershipLevel" class="yourMembershipLevelAttrib">
                                            <div
                                                style="float: left; line-height: 3; text-align: left; font-size: 10px; color: black;">
                                                Please see Players Club for more details.
                                            </div>
                                        </div>

                                        <div style="clear:both;"></div>
                                    </div>

                                    <div id="pointsBalanceSection">

                                        <div id="pointsBalanceAmtH">
                                            <input type="hidden" value="{{$data->tierCredits}}" id="pointsHidden">
                                        </div>

                                        <div id="eProgressBar" style="position: relative;">

                                            <div style="margin-top: 10px;">
                                                <div style="width: 70px; float: left;"><img
                                                        style="width: 55px; float:left;"
                                                        src="/HeaderFooterAssets/images/HR_Unity_Card_Star.png">
                                                </div>
                                                <div style="width: 20px; float: left;">&nbsp;</div>
                                                <div style="width: 70px; float: left;"><img
                                                        style="width: 55px; float:left;"
                                                        src="/HeaderFooterAssets/images/HR_Unity_Card_Legend.png">
                                                </div>
                                                <div style="width: 60px; float: left;">&nbsp;</div>
                                                <div style="width: 70px; float: left;"><img
                                                        style="width: 55px; float:left;"
                                                        src="/HeaderFooterAssets/images/HR_Unity_Card_Icon.png"></div>
                                                <div style="clear:both;"></div>
                                            </div>

                                            <div
                                                style="margin-top: 10px; margin-top: 5px; margin-bottom: 5px; font-size: 11px; color: black; line-height: 1;">
                                                <div style="width: 70px; float: left;">STAR</div>
                                                <div style="width: 20px; float: left;">&nbsp;</div>
                                                <div style="width: 70px; float: left;">LEGEND</div>
                                                <div style="width: 73px; float: left;">&nbsp;</div>
                                                <div style="width: 70px; float: left;">ICON</div>
                                                <div style="clear:both;"></div>
                                            </div>

                                            <div id="progressbar" style="position: relative; width: 335px;"
                                                 class="ui-progressbar ui-widget ui-widget-content ui-corner-all"
                                                 role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                 aria-valuenow="19">
                                                <div class="ui-progressbar-value ui-widget-header ui-corner-left"
                                                     style='display: block; width: 19%; border: 0px; margin: 0px; background: url("/HeaderFooterAssets/images/PremierArrowGradiant-11.png") right center no-repeat rgb(225, 84, 106);'></div>
                                            </div>

                                            <div id="progressbarArrow"
                                                 style="top: 55px; width: 32px; left: 355px; /*width: 32px;*/ height: 24px; position: absolute; z-index: 1;">
                                                <!-- <div id="progressbarArrowSpacer"></div> -->
                                                <div id="arrow-right"></div>
                                            </div>

                                            <div id="progressbarPremierArrow"
                                                 style="top: 55px; width: 32px; left: 110px; /*width: 32px;*/ height: 24px; position: absolute; z-index: 1;">
                                                <div id="arrow-right"></div>
                                            </div>

                                            <div id="progressbarPlatinumArrow"
                                                 style="top: 55px; width: 32px; left: 235px; /*width: 32px;*/ height: 24px; position: absolute; z-index: 1;">
                                                <div id="arrow-right"></div>
                                            </div>

                                            <div id="progressbarPlatinum"
                                                 style="position: absolute; top: 56px; /* display: none; */ width: 335px;"
                                                 class="ui-progressbar ui-widget ui-widget-content ui-corner-all"
                                                 role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                 aria-valuenow="0">
                                                <div class="ui-progressbar-value ui-widget-header ui-corner-left"
                                                     style="display: none; width: 2%; border: 0px; margin: 0px;"></div>
                                            </div>

                                            <div id="progressbarPremier"
                                                 style="position: absolute; top: 56px; /* display: none; */ width: 335px;"
                                                 class="ui-progressbar ui-widget ui-widget-content ui-corner-all"
                                                 role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                 aria-valuenow="0">
                                                <div class="ui-progressbar-value ui-widget-header ui-corner-left"
                                                     style="display: none; width: 2%; border: 0px; margin: 0px;"></div>
                                            </div>

                                            <div
                                                style="margin-top: 10px; margin-top: 5px; margin-bottom: 5px; font-size: 11px; color: #000000; line-height: 1; position: relative; /*top: -45px;*/ z-index: 1;">
                                                <div style="width: 60px; float: left;">0-3,999</div>
                                                <div style="width: 35px; float: left;">&nbsp;</div>
                                                <div style="width: 80px; float: left;">4,000 - 14,999</div>
                                                <div style="width: 45px; float: left;">&nbsp;</div>
                                                <div style="width: 80px; float: left;">15,000 +</div>
                                                <div style="clear:both;"></div>
                                            </div>

                                            <div
                                                style="margin-top: 10px; margin-top: 5px; margin-bottom: 5px; font-size: 11px; color: #000000; line-height: 1; position: relative; /*top: -46px;*/">
                                                <div style="width: 60px; float: left;">Tier Credits</div>
                                                <div style="width: 35px; float: left;">&nbsp;</div>
                                                <div style="width: 80px; float: left;">Tier Credits</div>
                                                <div style="width: 45px; float: left;">&nbsp;</div>
                                                <div style="width: 80px; float: left;">Tier Credits</div>
                                                <div style="clear:both;"></div>
                                            </div>

                                            <div>
                                                <div
                                                    style="float: left; line-height: 3; text-align: left; font-size: 12px; color: black;">
                                                    Points as of date: {{$data->AsOf_Date}}
                                                </div>
                                            </div>

                                            <div style="clear:both;"></div>
                                        </div>

                                        <div style="clear:both;"></div>
                                    </div>

                                    <div style="clear:both;"></div>
                                </div>

                                <div style="clear:both;"></div>
                            </div>
                        </div>
                        <script type="text/javascript" src="/HeaderFooterAssets/js/jquery-ui.js"></script>
                        <!--  END eStatement  -->
                    </div>
                    <title>Host Box</title>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">

                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/mos.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/host.css">

                    <style type="text/css">


                    </style>
                    <!--  START Host -->
                    <div id="hostContainer">
                        <div id="hostContainerInner">
                            <div id="host" style="display: block;">
                                <div id="hostPictureBox">
                                    <div id="hostPicture">
                                        <img id="hostPicImg" src="{{$data->hostImage}}">
                                    </div>
                                </div>
                                <div id="hostInfoBox">
                                    <div id="hostName">{{$data->hostMarketingName}}</div>
                                    <div id="hostTitle">{{$data->hostTitle}}</div>
                                    <br>

                                    <div id="hostEmail" style="font-size:13px;"><a href="mailto:{{$data->hostEmail}}"
                                                                                   style="word-break: break-all;">{{$data->hostEmail}}</a>
                                    </div>
                                    <div id="hostPhone" style="width:100%;text-align:center;font-size:13px">
                                        {{$data->hostPhone?'Office:'.$data->hostPhone:''}}
                                    </div>
                                    <div id="hostCell" style="display: none;">
                                        <div class="hostText">Mobile:</div>
                                        <div></div>
                                    </div>
                                    <!--
                                    <br>
                                    <div id="hostCell"><a href="https://" target="_blank">Live Chat</a></div>
                                     -->
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                        </div>
                    </div>

                    <div id="HideShow" style="clear:both;">

                        <script type="text/javascript">(function ($) {
                                'use strict';
                                $(function () {
                                    $('#hostPhone').show();
                                    $('#hostCell').hide();
                                });
                            })(jQuery);</script>


                    </div>
                    <!--  END Host -->

                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">

                    <title>Question</title>

                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/mos.css">
                    <link rel="stylesheet" type="text/css" href="/HeaderFooterAssets/css/questions.css">
                    <!-- Question Area Start -->
                    <div id="questionsContainer" style="display: none;">

                        <div id="questionTitle">
                            <div>
                                <h2>SURVEY QUESTION</h2>
                            </div>
                        </div>

                        <div id="questionsContainerInner">

                            <div id="questionBox" style="">

                                <div id="questionBoxTitle"><h3>What Do You Think?</h3></div>

                                <div id="question">
                                    <div style="margin: 10px; display: none;">
                                        <img id="questionBoxImg" src="" style="">
                                    </div>

                                    <div id="errorMessage" style="">

                                    </div>

                                    <div id="questionBoxQuestion">
                                        <!--  The question you want to ask goes here in this section we need to add more -->


                                    </div>

                                    <div style="clear: both;"></div>

                                </div>

                                <div id="questionButtons" class="radio_contain">
                                    <div class="questionButtonsInner">
                                        <div class="questionButtonsTable">
                                            <form id="formQ" method="post" action="./qa">

                                            </form>

                                        </div>
                                    </div>

                                    <form id="formSkipQ" method="post" action="./qa">
                                        <input type="hidden" id="skip" name="skip" value="skip">
                                    </form>

                                    <div id="submitButton" style="">
                                        <a id="ContinueButton" style="" href="#"
                                           onclick="document.getElementById('formQ'    ).submit();">Vote</a>
                                    </div>

                                    <div id="skipButton" style="">
                                        <a id="SkipButton" style="" href="#"
                                           onclick="document.getElementById('formSkipQ').submit();">Skip</a>
                                    </div>

                                </div>

                                <div style="clear: both;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Question Area End -->

                    <div id="HideShow" style="clear:both;">
                        <script type="text/javascript">(function ($) {
                                'use strict';
                                $(function () {
                                    $('#questionsContainer').hide();
                                });
                            })(jQuery);</script>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $("#progressbarPremier").progressbar({
                value: 0
            });
            $("#progressbarPlatinum").progressbar({
                value: 0
            });
            $("#progressbarArrows").progressbar({
                value: 0
            });
            $("#progressbar").progressbar({
                value: 0
            });

            $('.ui-progressbar-value').css("border", "0");
            $('.ui-progressbar-value').css("margin", "0");

            var vLevel = $('#pointsHidden').val();
            /*  for testing */
            //vLevel  =  0;
            //vLevel  =  500;
            //vLevel  =  999;

            //vLevel  =  1000;
            //vLevel  =  7000;
            // vLevel  =  14000;

            //vLevel  =  15000;
            //vLevel  =  42000;
            //vLevel  =  99000;

            // vLevel  =  110000;

            //vLevel  =  374;

            //vLevel  =  3500;
            console.log(vLevel);
            if (vLevel === 0) {
                // continue
            }
            //else if  ( vLevel < 1000 ) {
            //else if  ( vLevel < 750 ) {
            else if (vLevel < 4000) {
                $("#progressbar > div").css({'background': 'url(/HeaderFooterAssets/images/PremierArrowGradiant-11.png	) 	#e1546a right no-repeat'});
            }
            //else if ( vLevel < 15000 ) {
            //else if ( vLevel < 11250 ) {
            else if (vLevel < 15000) {
                $("#progressbar > div").css({'background': 'url(/HeaderFooterAssets/images/PlatinumArrowGradiant-11.png	)	#d1d1d1 right no-repeat'});
            }
            //else if ( vLevel >= 15000 ) {
            //else if ( vLevel >= 11250 ) {
            else if (vLevel >= 15000) {
                if (vLevel < 99000) {
                    $("#progressbar > div").css({'background': 'url(/HeaderFooterAssets/images/EliteArrowGradiant-11.png		)  	#9262a6 right no-repeat'});
                } else {
                    $("#progressbar         > div").css({'background': 'url(/HeaderFooterAssets/images/EliteGradiant.jpg		)  	#9262a6 repeat'});
                    $("#arrow-right").css({'background': 'url(/HeaderFooterAssets/images/ProgressBarPurpleArrowRightPurpleBack-11.png) no-repeat'});
                }
            } else {
                $("#progressbar > div").css({'background': 'url(/HeaderFooterAssets/images/PremierArrowGradiant-11.png	)  	#e1546a right no-repeat'});
            }
            if (vLevel <= 3999) {

                if (vLevel === 0) {
                    // continue
                } else if ((3999 - vLevel) === 0) {
                    vLevel = 28;
                } else {
                    vLevel = (vLevel / 3999) * 28;
                }

                // Next highest Level
                vLevel = Math.ceil(vLevel);
            } else if (vLevel <= 14999) {
                if ((14999 - vLevel) === 0) {
                    vLevel = 65;
                } else {
                    vLevel = vLevel - 3999;
                    vLevel = ((vLevel / 10999) * 37);
                    vLevel = vLevel + 28;
                    if (vLevel < 30) {
                        vLevel = 30;
                    }
                }
                // Next highest Level
                vLevel = Math.ceil(vLevel);
            } else if (vLevel >= 15000) {
                if (vLevel >= 500000) {
                    vLevel = 100;
                } else if ((vLevel - 15000) === 0) {
                    vLevel = 66;
                } else {
                    vLevel = vLevel - 14999;
                    vLevel = (vLevel / 500000) * 35;
                    vLevel = vLevel + 65;
                }

                // Next highest Level
                vLevel = Math.ceil(vLevel);
            }

            // alert("level=" + vLevel);
            $("#progressbar").progressbar("option", "value", parseInt(vLevel));

            // Max previous levels out

            $("#progressbarPremier").progressbar("option", "value", vLevel);
            $("#progressbarPremier  > div").css({'background': 'url(/HeaderFooterAssets/images/PremierArrowGrayBackGradiant-11.png	) #e1546a right no-repeat'});
        });
    </script>
@endsection


